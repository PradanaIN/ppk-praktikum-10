# PPK-Praktikum 10 : Event Handling

## Identitas

```
Nama : Novanni Indi Pradana
NIM  : 222011436
Kelas: 3SI3

```

## Deskripsi

Terdapat dua cara untuk menangani event pada pemrograman android, yang pertama adalah dengan mendaftarkan event pada xml activity. Yang kedua adalah dengan menambahkannya secara dinamis pada file java activity. 

## Kegiatan Praktikum

### 1. Hello World
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-10/-/raw/main/Screenshot/Screenshot%20(1).png)
### 2. Button Clicked
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-10/-/raw/main/Screenshot/Screenshot%20(2).png)
### 3. Button 2 Clicked
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-10/-/raw/main/Screenshot/Screenshot%20(3).png)

## Penugasan Praktikum : Kalkulator Luas Persegi

### 1. Tampilan Awal Kalkulator
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-10/-/raw/main/Screenshot/Screenshot%20(4).png)
### 3. Operasi Kalkulator
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-10/-/raw/main/Screenshot/Screenshot%20(5).png)
